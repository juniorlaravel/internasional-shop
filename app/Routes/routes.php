<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', [
    'uses' => 'Auth\LoginController@get',
    'as' => 'login_page',
]);

Route::post('login', [
    'uses' => 'Auth\LoginController@post',
    'as' => 'create_session',
]);

Route::match(array('GET', 'POST'), 'logout', [
    'uses' => 'Auth\LoginController@logout',
    'as' => 'logout',
]);


Route::get('forgot_password', [
    'uses' => 'Auth\ForgotPasswordController@get',
    'as' => 'password.request',
]);

Route::post('forgot_password', [
    'uses' => 'Auth\ForgotPasswordController@post',
    'as' => 'password.email',
]);

Route::get('password/reset/{token}', [
    'uses' => 'Auth\ResetPasswordController@get',
    'as' => 'password.reset',
]);

Route::get('register', [
    'uses' => 'Auth\RegisterController@get',
    'as' => 'register',
]);

Route::post('register', [
    'uses' => 'Auth\RegisterController@post',
    'as' => 'create_credential',
]);

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [
    'uses' => 'HomeController@index',
    'as' => 'home',
]);

Route::group(['middleware' => 'auth'], function () {
  // All route your need authenticated
});
