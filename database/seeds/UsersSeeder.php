<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // membuat role admin
        App\User::create([
            'name' => 'admin',
            'email' => 'admin@merahputih.com',
            'password' => bcrypt('admin'),
            'role' => 'admin'
        ]);

        //membuat role developer
        App\User::create([
            'name' => 'developer',
            'email' => 'dev@merahputih.com',
            'password' => bcrypt('develop'),
            'role' => 'developer'
        ]);

        //membuat role pelanggan
        App\User::create([
            'name' => 'pelanggan',
            'email' => 'pelanggan@merahputih.com',
            'password' => bcrypt('pelanggan'),
            'role' => 'pelanggan'
        ]);
        
    }
}
