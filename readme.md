# Jamur Merah Putih 

* Melayani berbagai jenis penjualan jamur ke kluar negri
* pembayaran menggunakan paypal
* pengirimangan menggunakan JNE ke Jasa Transhipper
* Pengiriman ke customer dari menggunakan Transhipper masing masing tujuan

## Jenis Proyek

* Proyek pengengembangan jangka panjang 
* Proyek investasi dengan income fee $5 per transaksi bagi developer

## Feature Sytem

* Penjual Memegang penuh Admin
* Develop Memegang penuh akses Develop dan laporan transaksi
* Manajemen Produk
* Penjualan 
* Katalog
* Pemesanan
* Manajemen Pemesanan
* Pembelian
* Pembayaran (dengan paypal atau Wastren Union)
* Cek Pemesanan
* Cek Snipping
* Laporan
* Pembayaran dengan paypal atau Wastren Union
* Fitur Real Chating

## Object
* Home Industri Yang Penjualannya Ke Luarnegri melalui transhipper
* Bisnis Jual Beli Non PT dan CV dengan MOU seperti penjelasan di jenis project

##How To Install
* run command 'composer update'
* run command 'npm-install'
* copy .env.example and rename to .env
* run php artisan key:generate
* run php artisan migrate