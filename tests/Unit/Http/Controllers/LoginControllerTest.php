<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;

use URL;

class LoginControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_post_when_not_yet_login()
    {
        $path = URL::route('login_page');

        $response_status = $this->get($path)->status();
        $this->assertEquals(200, $response_status);
    }

    public function test_post_when_login_already()
    {
        $path = URL::route('login_page');

        $response = $this->get($path);
        $this->assertRedirectedTo('root');
    }
}
